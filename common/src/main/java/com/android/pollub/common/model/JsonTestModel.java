package com.android.pollub.common.model;

/**
 * Created by msienczyk on 22.05.2017.
 */

public class JsonTestModel {

    private TestListModel handheld, wearable;

    public JsonTestModel(TestListModel handheld, TestListModel wearable) {
        this.handheld = handheld;
        this.wearable = wearable;
    }

}
