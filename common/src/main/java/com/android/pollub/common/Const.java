package com.android.pollub.common;

/**
 * Created by msienczyk on 22.05.2017.
 */

public class Const {

    public static final String PREPARE_WEAR_SERVICE = "/prepare_wear_service";
    public static final String START_WEAR_SERVICE = "/start_wear_service";
    public static final String STOP_WEAR_SERVICE = "/stop_wear_service";

    public static final String TEST_OBJ_PATH = "/test";
    public static final String TEST_OBJ_NAME = "wearTest";

}
