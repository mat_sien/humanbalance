package com.android.pollub.common;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;

import com.android.pollub.common.model.FileModel;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by msienczyk on 23.11.2016.
 */

public class FileManager {

    private static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public static File getFolder() {
        File folder = null;
        if (isExternalStorageWritable()) {
            folder = new File(Environment.getExternalStorageDirectory() + "/HumanBalance");
            boolean success = true;
            if (!folder.exists()) {
                success = folder.mkdir();
            }
            if (!success) {
                folder = null;
            }
        }
        return folder;
    }

    public static ArrayList<FileModel> getTestsList() {

        ArrayList<FileModel> fileArrayList = new ArrayList<>();

        File directory = FileManager.getFolder();

        File[] list = directory.listFiles();

//        Arrays.sort(list, Collections.reverseOrder());

        Arrays.sort(list, new Comparator<File>() {
            public int compare(File f1, File f2) {
                return Long.compare(f2.lastModified(), f1.lastModified());
            }
        });

        for (File file : list) {
            fileArrayList.add(new FileModel(file));
        }

        return fileArrayList;
    }

    public static void sendFile(Context context, Uri uri) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_STREAM, uri);
        context.startActivity(Intent.createChooser(i, "Send:"));
    }

    public static File saveFile(String json, String fileName) throws IOException {

        File folder = getFolder();

        if (folder != null) {

            Writer output = null;
            File file = new File(folder + "/" + fileName + ".json");
            output = new BufferedWriter(new FileWriter(file));
            output.write(json);
            output.close();

            return file;
        } else {
            return null;
        }
    }

    public static boolean deleteFile(String fileName) {

        File folder = getFolder();

        if (folder != null) {

            File file = new File(folder + "/" + fileName + ".json");
            return file.delete();

        } else {
            return false;
        }
    }

    public static boolean deleteFile(Uri uri) {

        File folder = getFolder();

        if (folder != null) {

            File file = new File(uri.getPath());
            return file.delete();

        } else {
            return false;
        }
    }

}
