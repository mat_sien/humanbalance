package com.android.pollub.common.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by msienczyk on 25.11.2016.
 */

public class TestModel implements Serializable {

    private long seconds;
    private double frequency;
    private ArrayList<XYZSampleModel> sampleList;
    private long startTime, endTime;

    public TestModel() {
        sampleList = new ArrayList<>();
        startTime = System.currentTimeMillis();
    }

    public void stopTest() {
        endTime = System.currentTimeMillis();
        seconds = getSeconds();
        frequency = getFrequency();
    }

    public void addSample(XYZSampleModel sample) {
        sampleList.add(sample);
    }

    public long getTimeInMillis() {
        return endTime - startTime;
    }

    public long getSeconds() {
        return getTimeInMillis() / 1000;
    }

    public double getFrequency() {
        return sampleList.size() / (getTimeInMillis() / 1000.0);
    }

    @Override
    public String toString() {
        return "seconds=" + seconds +
                ", frequency=" + frequency +
                ", sampleNumber=" + sampleList.size();
    }

}
