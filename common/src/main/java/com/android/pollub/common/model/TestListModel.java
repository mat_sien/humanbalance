package com.android.pollub.common.model;

import java.io.Serializable;

/**
 * Created by msienczyk on 25.11.2016.
 */

public class TestListModel implements Serializable {

    private TestModel accelerometerTest, gyroscopeTest, gravityTest, magneticFieldTest, linearAccelerometerTest;

    public TestListModel(TestModel accelerometerTest, TestModel gyroscopeTest, TestModel gravityTest, TestModel magneticFieldTest, TestModel linearAccelerometerTest) {
        this.accelerometerTest = accelerometerTest;
        this.gyroscopeTest = gyroscopeTest;
        this.gravityTest = gravityTest;
        this.magneticFieldTest = magneticFieldTest;
        this.linearAccelerometerTest = linearAccelerometerTest;
    }

    @Override
    public String toString() {
        return "TestListModel{" +
                "accelerometerTest=" + accelerometerTest.toString() +
                ", gyroscopeTest=" + gyroscopeTest.toString() +
                '}';
    }
}
