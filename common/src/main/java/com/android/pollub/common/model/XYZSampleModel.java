package com.android.pollub.common.model;

import java.io.Serializable;

/**
 * Created by msienczyk on 25.11.2016.
 */

public class XYZSampleModel implements Serializable {

    private float x, y, z;
    private long sampleDate;

    public XYZSampleModel(float[] values) {
        x = values[0];
        y = values[1];
        z = values[2];
        sampleDate = System.currentTimeMillis();
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

    public long getSampleDate() {
        return sampleDate;
    }
}
