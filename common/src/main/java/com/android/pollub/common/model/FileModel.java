package com.android.pollub.common.model;

import android.net.Uri;

import java.io.File;

/**
 * Created by msienczyk on 23.11.2016.
 */

public class FileModel {

    private String name;
    private Uri uri;

    public FileModel(File file){
        name = file.getName();
        uri = Uri.fromFile(file);
    }

    public String getName() {
        return name;
    }

    public Uri getUri() {
        return uri;
    }
}
