package com.android.pollub.humanbalance;

import android.content.Intent;

import com.android.pollub.common.Const;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

/**
 * Created by msienczyk on 22.05.2017.
 */

public class WearListenerService extends WearableListenerService {

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        super.onMessageReceived(messageEvent);
        if (messageEvent.getPath().equals(Const.PREPARE_WEAR_SERVICE)) {
            startService(new Intent(this, WearSensorService.class).setAction(WearSensorService.ACTION_PREPARE));
        } else if (messageEvent.getPath().equals(Const.START_WEAR_SERVICE)) {
            startService(new Intent(this, WearSensorService.class).setAction(WearSensorService.ACTION_START));
        } else if (messageEvent.getPath().equals(Const.STOP_WEAR_SERVICE)) {
            startService(new Intent(this, WearSensorService.class).setAction(WearSensorService.ACTION_STOP));
        }
    }
}
