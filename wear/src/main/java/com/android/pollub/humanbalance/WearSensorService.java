package com.android.pollub.humanbalance;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.android.pollub.common.Const;
import com.android.pollub.common.Utils;
import com.android.pollub.common.model.TestListModel;
import com.android.pollub.common.model.TestModel;
import com.android.pollub.common.model.XYZSampleModel;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import java.io.IOException;

/**
 * Created by msienczyk on 22.05.2017.
 */

public class WearSensorService extends Service implements GoogleApiClient.ConnectionCallbacks {


    private final static String TAG = "WearSensorService";

    public static final String ACTION_PREPARE = "action_prepare";
    public static final String ACTION_START = "action_start";
    public static final String ACTION_STOP = "action_stop";

    private GoogleApiClient googleApiClient;
    private TestModel accelerometerTest, gyroscopeTest, magneticFieldTest, gravityTest, linearAccelerometerTest;
    private Sensor gyroscope, accelerometer, magneticField, gravity, linearAccelerometer;
    private SensorManager sensorManager;
    private SensorEventListener accelerometerListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {

            XYZSampleModel accSample = new XYZSampleModel(sensorEvent.values);

            accelerometerTest.addSample(accSample);

        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    };

    private SensorEventListener gyroscopeListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {

            XYZSampleModel gyroSample = new XYZSampleModel(sensorEvent.values);

            gyroscopeTest.addSample(gyroSample);

        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    };

    private SensorEventListener magneticFieldListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {

            XYZSampleModel sample = new XYZSampleModel(sensorEvent.values);

            magneticFieldTest.addSample(sample);

        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    };

    private SensorEventListener gravityListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {

            XYZSampleModel sample = new XYZSampleModel(sensorEvent.values);

            gravityTest.addSample(sample);

        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    };

    private SensorEventListener linearAccelerometerListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {

            XYZSampleModel sample = new XYZSampleModel(sensorEvent.values);

            linearAccelerometerTest.addSample(sample);

        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    };


    @Override
    public void onCreate() {
        super.onCreate();

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        gyroscope = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        gravity = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        magneticField = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        linearAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(Wearable.API)
                .build();

        googleApiClient.connect();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent.getAction().equals(ACTION_START)) {
            startTest();
        } else if (intent.getAction().equals(ACTION_STOP)) {
            stopTest();
        }

        return START_NOT_STICKY;
    }

    private void startTest() {

        sensorManager.registerListener(accelerometerListener, accelerometer, SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(gyroscopeListener, gyroscope, SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(gravityListener, gravity, SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(magneticFieldListener, magneticField, SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(linearAccelerometerListener, linearAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);

        accelerometerTest = new TestModel();
        gyroscopeTest = new TestModel();
        gravityTest = new TestModel();
        magneticFieldTest = new TestModel();
        linearAccelerometerTest = new TestModel();
    }

    private void stopTest() {

        sensorManager.unregisterListener(accelerometerListener);
        sensorManager.unregisterListener(gyroscopeListener);
        sensorManager.unregisterListener(gravityListener);
        sensorManager.unregisterListener(magneticFieldListener);
        sensorManager.unregisterListener(linearAccelerometerListener);

        accelerometerTest.stopTest();
        gyroscopeTest.stopTest();
        gravityTest.stopTest();
        magneticFieldTest.stopTest();
        linearAccelerometerTest.stopTest();

        TestListModel wearTestList = new TestListModel(accelerometerTest, gyroscopeTest, gravityTest, magneticFieldTest, linearAccelerometerTest);

        try {
            byte[] bytes = Utils.serialize(wearTestList);

            Asset asset = Asset.createFromBytes(bytes);

            PutDataMapRequest dataMap = PutDataMapRequest.create(Const.TEST_OBJ_PATH);
            dataMap.getDataMap().putAsset(Const.TEST_OBJ_NAME, asset);
            PutDataRequest request = dataMap.asPutDataRequest();
            Wearable.DataApi.putDataItem(googleApiClient, request);
        } catch (IOException e) {
            e.printStackTrace();
        }

        accelerometerTest = null;
        gyroscopeTest = null;
        gravityTest = null;
        magneticFieldTest = null;
        linearAccelerometerTest = null;

        stopSelf();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        googleApiClient.disconnect();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}
