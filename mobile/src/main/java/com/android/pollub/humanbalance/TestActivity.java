package com.android.pollub.humanbalance;

import android.content.DialogInterface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.pollub.common.Const;
import com.android.pollub.common.FileManager;
import com.android.pollub.common.Utils;
import com.android.pollub.common.model.JsonTestModel;
import com.android.pollub.common.model.TestListModel;
import com.android.pollub.common.model.TestModel;
import com.android.pollub.common.model.XYZSampleModel;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class TestActivity extends AppCompatActivity implements DataApi.DataListener, GoogleApiClient.ConnectionCallbacks {

    private final static String TAG = "TestActivity";
    private final static int NUMBER_OF_PLACES = 2;

    long stopwatchTimeInMilliseconds = 0L;
    private TestModel accelerometerTest, gyroscopeTest, magneticFieldTest, gravityTest, linearAccelerometerTest;
    private Sensor gyroscope, accelerometer, magneticField, gravity, linearAccelerometer;
    private SensorManager sensorManager;
    private TextView timer;
    private Button testButton;
    private boolean isTestActive = false;
    private String fileName;
    private TestListModel handheldTestList = null, wearableTestList = null;
    private long startTime = 0L;
    private TextView value1, value2, value3, testName;
    private RelativeLayout progressBar;
    private AlertDialog inputNameAlertDialog;
    private Handler customHandler = new Handler();
    private GoogleApiClient googleApiClient;
    private SensorEventListener accelerometerListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {

            XYZSampleModel accSample = new XYZSampleModel(sensorEvent.values);

            accelerometerTest.addSample(accSample);

//            value1.setText(Utils.round(accSample.getX(), NUMBER_OF_PLACES) + "m/s2");
//            value2.setText(Utils.round(accSample.getY(), NUMBER_OF_PLACES) + "m/s2");
//            value3.setText(Utils.round(accSample.getZ(), NUMBER_OF_PLACES) + "m/s2");

        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    };

    private SensorEventListener gyroscopeListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {

            XYZSampleModel gyroSample = new XYZSampleModel(sensorEvent.values);

            gyroscopeTest.addSample(gyroSample);

        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    };

    private SensorEventListener magneticFieldListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {

            XYZSampleModel sample = new XYZSampleModel(sensorEvent.values);

            magneticFieldTest.addSample(sample);

        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    };

    private SensorEventListener linearAccelerometerListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {

            XYZSampleModel sample = new XYZSampleModel(sensorEvent.values);

            linearAccelerometerTest.addSample(sample);

        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    };

    private SensorEventListener gravityListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {

            XYZSampleModel sample = new XYZSampleModel(sensorEvent.values);

            gravityTest.addSample(sample);

        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    };

    private Runnable updateTimerThread = new Runnable() {

        public void run() {

            stopwatchTimeInMilliseconds = System.currentTimeMillis() - startTime;

            int secs = (int) (stopwatchTimeInMilliseconds / 1000);
            int mins = secs / 60;
            secs = secs % 60;
            int milliseconds = (int) (stopwatchTimeInMilliseconds % 1000);
            timer.setText(mins + ":"
                    + String.format("%02d", secs) + ":"
                    + String.format("%03d", milliseconds));
            customHandler.postDelayed(this, 0);
        }

    };

    private View.OnClickListener testNameOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showTestNameDialog();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.perform_test);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressBar = (RelativeLayout) findViewById(R.id.progressBarLayout);

        timer = (TextView) findViewById(R.id.timer);

//        value1 = (TextView) findViewById(R.id.value1);
//        value2 = (TextView) findViewById(R.id.value2);
//        value3 = (TextView) findViewById(R.id.value3);

        testName = (TextView) findViewById(R.id.test_name);
        testName.setOnClickListener(testNameOnClickListener);

        testButton = (Button) findViewById(R.id.test_button);
        testButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleTestButton();
            }
        });

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        gyroscope = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        gravity = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        magneticField = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        linearAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(Wearable.API)
                .build();

        googleApiClient.connect();

        inputNameAlertDialog = createDialog().create();
        inputNameAlertDialog.setCanceledOnTouchOutside(true);
        showTestNameDialog();
    }

    private void showTestNameDialog() {
        inputNameAlertDialog.show();
        inputNameAlertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    private AlertDialog.Builder createDialog() {

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setHint(R.string.enter_test_name);

        int margin = getResources().getDimensionPixelSize(R.dimen.dialog_margin);

        return getProperAlertDialogBuilder()
                .setView(input, margin, margin, margin, 0)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String text = input.getText().toString();
                        if (!text.isEmpty()) {
                            fileName = text;
                            testName.setText(text);
                        }
                        input.setText("");
                        dialog.cancel();
                    }
                });
    }

    private AlertDialog.Builder getProperAlertDialogBuilder() {
        return new AlertDialog.Builder(this);
    }

    private void handleTestButton() {
        if (!isTestActive) {
            testButton.setText(getString(R.string.stop));
            startTest();
        } else {
            testButton.setText(getString(R.string.start));
            stopTest();
        }
    }

    private void showProgressBar(String text) {
        TextView progressBarText = (TextView) progressBar.findViewById(R.id.progressDialogText);
        progressBarText.setText(text);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    private void startTest() {
        sendMessage(Const.START_WEAR_SERVICE);

        System.gc();

        if (fileName != null) {
            fileName = fileName + "_" + Utils.getFormattedCurrentDate();
        } else {
            fileName = Utils.getFormattedCurrentDate();
        }
        testName.setText(fileName);
        fileName = null;

        isTestActive = true;
        testName.setOnClickListener(null);

        resetAndStartStopwatch();

        sensorManager.registerListener(accelerometerListener, accelerometer, SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(gyroscopeListener, gyroscope, SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(gravityListener, gravity, SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(magneticFieldListener, magneticField, SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(linearAccelerometerListener, linearAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);

        accelerometerTest = new TestModel();
        gyroscopeTest = new TestModel();
        gravityTest = new TestModel();
        magneticFieldTest = new TestModel();
        linearAccelerometerTest = new TestModel();
    }

    private void stopTest() {
        isTestActive = false;
        testName.setOnClickListener(testNameOnClickListener);

        sendMessage(Const.STOP_WEAR_SERVICE);

        stopStopwatch();

        sensorManager.unregisterListener(accelerometerListener);
        sensorManager.unregisterListener(gyroscopeListener);
        sensorManager.unregisterListener(gravityListener);
        sensorManager.unregisterListener(magneticFieldListener);
        sensorManager.unregisterListener(linearAccelerometerListener);

        accelerometerTest.stopTest();
        gyroscopeTest.stopTest();
        gravityTest.stopTest();
        magneticFieldTest.stopTest();
        linearAccelerometerTest.stopTest();

        handheldTestList = new TestListModel(accelerometerTest, gyroscopeTest, gravityTest, magneticFieldTest, linearAccelerometerTest);


        saveJson();

        accelerometerTest = null;
        gyroscopeTest = null;
        gravityTest = null;
        magneticFieldTest = null;
        linearAccelerometerTest = null;

    }

    private File saveJson() {

        showProgressBar(getString(R.string.saving_test));

        JsonTestModel jsonTest = new JsonTestModel(handheldTestList, wearableTestList);

        Gson gson = new Gson();
        String json = gson.toJson(jsonTest);

        File file = null;
        String testNameString = testName.getText().toString();

        try {
            if (wearableTestList != null) {
                FileManager.deleteFile(testNameString);
                file = FileManager.saveFile(json, testNameString + "_withWear");
            } else {
                file = FileManager.saveFile(json, testNameString);
            }
        } catch (IOException e) {
            Log.e(TAG, "Failed to save");
            e.printStackTrace();
        } finally {
            hideProgressBar();
        }

        Toast toast = Toast.makeText(this, R.string.test_saved_success, Toast.LENGTH_SHORT);
        if (file == null) {
            toast.setText(R.string.test_saved_failture);
        }
        toast.show();

        return file;
    }

    private void sendMessage(final String command) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(googleApiClient).await();
                for (Node node : nodes.getNodes()) {
                    Wearable.MessageApi.sendMessage(googleApiClient, node.getId(), command, null).await();
                }
            }
        }).start();
    }

    private void resetAndStartStopwatch() {
        stopwatchTimeInMilliseconds = 0L;

        startTime = System.currentTimeMillis();
        customHandler.postDelayed(updateTimerThread, 0);
    }

    private void stopStopwatch() {
        customHandler.removeCallbacks(updateTimerThread);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        sendMessage(Const.PREPARE_WEAR_SERVICE);
        Wearable.DataApi.addListener(googleApiClient, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Wearable.DataApi.removeListener(googleApiClient, this);
        googleApiClient.disconnect();
        inputNameAlertDialog.cancel();
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEventBuffer) {

        showProgressBar(getString(R.string.receiving_data));

        for (DataEvent event : dataEventBuffer) {
            if (event.getType() == DataEvent.TYPE_CHANGED && event.getDataItem().getUri().getPath().equals(Const.TEST_OBJ_PATH)) {
                DataMapItem dataMapItem = DataMapItem.fromDataItem(event.getDataItem());
                Asset asset = dataMapItem.getDataMap().getAsset(Const.TEST_OBJ_NAME);
                new LoadTestFromAsset().execute(asset);
            }
        }
    }

    private class LoadTestFromAsset extends AsyncTask<Object, Void, TestListModel> {

        private Asset asset;

        @Override
        protected TestListModel doInBackground(Object... params) {

            asset = (Asset) params[0];
            wearableTestList = loadTestFromAsset(asset);

            return wearableTestList;
        }

        @Override
        protected void onPostExecute(TestListModel testList) {
            super.onPostExecute(testList);

            saveJson();
        }

        private TestListModel loadTestFromAsset(Asset asset) {
            if (asset == null) {
                throw new IllegalArgumentException("Asset must be non-null");
            }
            // convert asset into a file descriptor and block until it's ready
            InputStream assetInputStream = Wearable.DataApi.getFdForAsset(googleApiClient, asset).await().getInputStream();

            if (assetInputStream == null) {
                Log.w(TAG, "Requested an unknown Asset.");
                return null;
            }

            TestListModel testList = null;

            try {
                testList = Utils.deserializeFromInputStream(assetInputStream, TestListModel.class);
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }

            return testList;
        }
    }
}
