package com.android.pollub.humanbalance;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.util.Pair;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Patterns;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.pollub.common.FileManager;
import com.android.pollub.common.model.FileModel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ListActivity extends AppCompatActivity {

    private final static String OK = "0";
    private final static String FAIL = "-1";

    private ArrayList<FileModel> fileList;
    private ListView listView;
    private FileListAdapter adapter;

    private RelativeLayout progressBar;
    private AlertDialog inputServerAddressDialog;

    private String serverUrl;
    private SharedPreferences sharedPref;

    private AdapterView.OnItemClickListener onListClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            view.showContextMenu();
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.test_list);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedPref = getPreferences(Context.MODE_PRIVATE);
        serverUrl = sharedPref.getString(getString(R.string.server_address), "");

        progressBar = (RelativeLayout) findViewById(R.id.progressBarLayout);

        listView = (ListView) findViewById(R.id.testList);
        listView.setEmptyView(findViewById(R.id.emptyMessage));
        fileList = FileManager.getTestsList();
        adapter = new FileListAdapter(this, fileList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(onListClickListener);
        registerForContextMenu(listView);

        inputServerAddressDialog = createDialog().create();
        inputServerAddressDialog.setCanceledOnTouchOutside(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        inputServerAddressDialog.cancel();
    }

    private AlertDialog.Builder createDialog() {

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        input.setHint(R.string.server_address_hint);
        if (!serverUrl.isEmpty()) {
            input.setText(serverUrl);
        } else {
            input.setText("http://");
        }
        input.setSelection(input.getText().length());

        int margin = getResources().getDimensionPixelSize(R.dimen.dialog_margin);

        return getProperAlertDialogBuilder()
                .setTitle(R.string.enter_server_address)
                .setView(input, margin, margin, margin, 0)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        String text = input.getText().toString();
                        input.setSelection(input.getText().length());

                        if (!Patterns.WEB_URL.matcher(text).matches()) {
                            Toast.makeText(ListActivity.this, R.string.incorrect_url, Toast.LENGTH_SHORT).show();
                        } else {
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putString(getString(R.string.server_address), text);
                            editor.commit();
                            serverUrl = text;
                            dialog.cancel();
                        }
                    }
                });
    }

    private AlertDialog.Builder getProperAlertDialogBuilder() {
        return new AlertDialog.Builder(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.list_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.server_address:
                inputServerAddressDialog.show();
                inputServerAddressDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.test_item_contextual_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        FileModel fileModel = adapter.getItem(info.position);

        switch (item.getItemId()) {
            case R.id.menu_email:
                FileManager.sendFile(ListActivity.this, fileModel.getUri());
                break;
            case R.id.menu_server:
                if (!Patterns.WEB_URL.matcher(serverUrl).matches()) {
                    Toast.makeText(this, R.string.incorrect_url, Toast.LENGTH_SHORT).show();
                } else {
                    new UploadTest().execute(fileModel.getUri(), serverUrl);
                }
                break;
            case R.id.menu_delete:
                handleDeleteItem(fileModel);
                break;
        }

        return true;
    }

    private void handleDeleteItem(FileModel fileModel) {
        FileManager.deleteFile(fileModel.getUri());
        fileList = FileManager.getTestsList();
        adapter = new FileListAdapter(this, fileList);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void handleResponse(Pair<String, Integer> response) {

        if (response != null) {
            String code = response.first;
            int serverCode = response.second;

            if (code.equals(OK)) {
                Toast.makeText(this, R.string.upload_ok, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getString(R.string.upload_failed) + ", server code: " + serverCode, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, R.string.upload_failed, Toast.LENGTH_SHORT).show();
        }
    }

    private void showProgressBar(String text) {
        TextView progressBarText = (TextView) progressBar.findViewById(R.id.progressDialogText);
        progressBarText.setText(text);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    private class UploadTest extends AsyncTask<Object, Void, Pair<String, Integer>> {

        private File file;
        private String serverUrl;

        @Override
        protected Pair<String, Integer> doInBackground(Object... params) {

            Uri uri = (Uri) params[0];
            serverUrl = (String) params[1];

            file = new File(uri.getPath());

            Pair<String, Integer> myResponse = null;
            try {
                Response response = upload(serverUrl, file);
                myResponse = new Pair<>(response.body().string(), response.code());
            } catch (IOException e) {
                e.printStackTrace();
            }

            return myResponse;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar(getString(R.string.uploading));
        }

        @Override
        protected void onPostExecute(Pair<String, Integer> myResponse) {
            super.onPostExecute(myResponse);
            hideProgressBar();
            handleResponse(myResponse);
        }

        private Response upload(String serverUrl, File file) throws IOException {

            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(serverUrl + "/record")
                    .post(RequestBody.create(MediaType.parse("text/x-markdown; charset=utf-8"), file))
                    .build();

            return client.newCall(request).execute();
        }
    }
}
