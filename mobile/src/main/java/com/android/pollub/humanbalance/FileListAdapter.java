package com.android.pollub.humanbalance;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.pollub.common.model.FileModel;
import com.android.pollub.humanbalance.R;

import java.util.List;

/**
 * Created by msienczyk on 23.11.2016.
 */

public class FileListAdapter extends ArrayAdapter<FileModel> {

    public FileListAdapter(Context context, List<FileModel> objects) {
        super(context, 0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        FileModel file = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.test_row, parent, false);
        }
        TextView testName = (TextView) convertView.findViewById(R.id.testRow);
        testName.setText(file.getName());
        return convertView;
    }
}
